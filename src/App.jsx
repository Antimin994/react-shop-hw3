import { Route, Routes, Link, Outlet } from "react-router-dom"
import {useState} from "react";
import {Layout} from "./components/Layout/Layout"
import HomePage from "./components/Pages/HomePage"
import { useLocalStorage } from "./helpers/useLocalStorage"
import Favorite from "./components/Pages/Favorite/Favorite"
import Basket from "./components/Pages/Basket/Basket"



function App() {
	let [favorites, setFavorites] = useLocalStorage([], 'favorite');
	let [baskets, setBaskets] = useLocalStorage([], 'basket');
	let [currentItem, setCurrentItem] = useState([]);
	let [currentFavorite, setCurrentFavorite] = useState([]);
	const [isModalImage, setIsModalImage] = useState(false);
	const [isModalConfirmed, setIsModalConfirmed] = useState(false);
	const [isModal, setIsModal] = useState(false);


	const handleFavorites = (item) => {

		const isAdded = favorites.some((favorite)=> favorite.id === item.id)
		console.log(item, isAdded);

		if(isAdded){
			setFavorites(favorites.filter(i => i.id !== item.id))
		}
		else {
			setFavorites([...favorites,item])
		}
		
	}

	const handleBasket = (item) => {

		setIsModalConfirmed(false);
		setCurrentItem([]);
		if (baskets.some((basket)=> basket.id === item.id)) {
			baskets.forEach((basket, i, baskets) => {
				if (basket.id === item.id) {
					!basket.quantity ? basket.quantity = 2 : ++basket.quantity;
					
				}
			  })
			return setCurrentItem([...baskets])
		}

		setCurrentItem([...baskets,item])
	}

	const removeBasket = (item) => {
		setIsModalConfirmed(false);
		setCurrentItem([]);
		setCurrentItem(baskets.filter(i => i.id !== item.id))

	}

	/*const removeFavorites = (item) => {
		setIsModalConfirmed(false);
		setCurrentFavorite([]);
		setCurrentFavorite(favorites.filter(i => i.id !== item.id))

	}*/

	const handleConfirmed = () => {
		setIsModalConfirmed(!isModalConfirmed);
		setBaskets(currentItem);
		setFavorites(currentFavorite);
	}

    const handleModalImage = () => setIsModalImage(!isModalImage);
	const handleModal = () => setIsModal(!isModal);

	const minusItem = (item, quantity) => {
		if ((quantity === undefined) || (quantity === 1))  {
			removeBasket(item);
			handleModal()
		 } else {
			--item.quantity;
			baskets.forEach((arr, i, baskets) => {
				if (arr.id === item.id) {
					arr.quantity = item.quantity
				}
			  })
			  console.log('baskets', baskets);
			  setBaskets([...baskets]);
			console.log('quantity =>', item.quantity);	
		} 
		
	}
    const plusItem = (item, quantity) => {
		quantity === undefined ? item.quantity = 2 : ++item.quantity;
		baskets.forEach((arr, i, baskets) => {
			if (arr.id === item.id) {
				arr.quantity = item.quantity
			}
		  })
		  setBaskets([...baskets]);
	}
    

	return (
		<>
				<Routes>
				  <Route path="/" element={<Layout favorite={favorites} basket={baskets} />}>	
					<Route index element={<HomePage handleFavorites={handleFavorites} handleBasket={handleBasket} handleModalImage={handleModalImage} isModalImage={isModalImage} handleConfirmed={handleConfirmed} favorites={favorites} />}/>
					<Route path="/favorite" element={<Favorite favorites={favorites} handleFavorites={handleFavorites} />}/>
					<Route path="/basket" element={<Basket baskets={baskets} handleModal={handleModal} removeBasket={removeBasket} minusItem={minusItem} plusItem={plusItem} isModal={isModal} handleConfirmed={handleConfirmed} />}/>
				  </Route>
				</Routes>
			
		</>	
		
	)
}

export default App
