import {Link, Outlet} from "react-router-dom";
import { AiFillStar } from "react-icons/ai";
import { AiOutlineShopping } from "react-icons/ai";
import cx from "classnames"
import "../../styles/styles.scss"
import Logo from "./icons/logo.svg"

const Layout = ({favorite, basket}) =>{

    return(
      <div className="g-app">  
        <header className="g-header">
            <div className="container">
                <div className="header__wrapper">
                    <div className="header__logo">
                        <Link to="/" className="logo">
                        <img src="../../../src/components/Layout/icons/logo.svg"></img>
                        </Link>
                    </div>
                    <nav className="nav">
                        <ul>
                            <li><Link to="/">головна</Link></li>
                            <li><a href="#">магазин</a></li>
                            <li><a href="#">топ</a></li>
                            <li><a href="#">контакти</a></li>
                         </ul>
                    </nav>
                    <div className="header__actions">
                        <div className="header__actions_item">
                        <Link to="favorite"><AiFillStar className="favorite-item"></AiFillStar></Link>
                            <span className={cx("icon-favorite", {'icon-favorite_hidden':(favorite.length < 1)})}>
                                <span className="count">{favorite.length}</span>
                            </span>
                            
                        </div>
                        <div className="header__actions_item">
                        <Link to="/basket"><AiOutlineShopping className="favorite-item"></AiOutlineShopping></Link>
                            <span className={cx("icon-favorite", {'icon-favorite_hidden':(basket.length < 1)})}>
                                <span className="count">{basket.length}</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <main className="g-content">
            <Outlet />
        </main>
        <footer className="g-footer">
            <div className="copywriting">Developed by Anton Antimijchuk (c) 2023</div>
        </footer>
      </div>
    )
}


export {Layout}
