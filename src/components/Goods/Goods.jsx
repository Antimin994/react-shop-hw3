import Clothes from "./Clothes";
import Accessories from "./Accessories";
import LeftArrow from "../../icons/LeftArrow.svg?react"
import {Link} from "react-router-dom";
import {sendRequest} from "../../helpers/sendRequest";
import {useEffect, useState} from "react";
import './Goods.scss'

const Goods = ({item, handleFavorites, favorites, handleBasket, handleModalImage}) => {
	const [clothes, setClothes] = useState([])
	const [accessories, setAccessories] = useState([])
	const [isOpen,setIsOpen]=useState(false);
	const [currentPost, setCurrentPost] = useState({})
	const handleModal = () => setIsOpen(!isOpen)
	const handleCurrentPost = (cardPost) => setCurrentPost(cardPost)

	console.log(handleFavorites);
		
	
	useEffect(() => {
		sendRequest(`./../public/clothes.json`)
			.then((results) => {
				console.log("clothes",results);
				setClothes(results);
			})

		sendRequest(`./../public/accessories.json`)
			.then((results) => {
				console.log("acessories",results);
				setAccessories(results);
			})
	}, [])

	return (
		<div className="container">
			<div className="goods__container">
				<div className="goods__title">Одяг та взуття</div>
				<div className="goods__slider">
					<Clothes item={item} date={clothes} handleFavorites={handleFavorites} favorites={favorites} handleBasket={handleBasket} handleCurrentPost={handleCurrentPost} handleModalImage={handleModalImage} />
					<p className="goods__link"><Link to="/clothes/all" className="good-link"></Link></p>
				</div>
				<div className="goods__title">Аксесуари</div>
				<div className="goods__slider">
					<Accessories item={item} date={accessories} handleFavorites={handleFavorites} favorites={favorites} handleBasket={handleBasket} handleCurrentPost={handleCurrentPost} handleModalImage={handleModalImage} />
					<p className="goods__link"><Link to="/accessories/all" className="good-link"></Link></p>
				</div>
			</div>
		</div>
	)
}


export default Goods
