import Button from "../Button/Button";
import { AiFillStar } from "react-icons/ai";
import { AiOutlineStar } from "react-icons/ai";
import {useState} from "react";
import cx from "classnames";

const GoodsItem = ({item, handleModalImage, handleFavorites, favorites, handleBasket})=>{
    const {good_img, title, original_title, price, id, original_name, name } = item
    const linkPath = original_title ? `/cloth/${id}` : `/accessory/${id}`
    let isAdded = favorites.some((favorite)=> favorite.id === item.id)
    
    return (
        <>
        <div className="good-desc">
            <img src={good_img} height={'376px'} alt={title ? title : name}/>
            <AiFillStar onClick={()=>handleFavorites(item)} className={cx("good-desc__favorite", {'good-desc__favorite_hidden':(!isAdded)})} />
            <AiOutlineStar onClick={()=>handleFavorites(item)}  className={cx("good-desc__favorite", {'good-desc__favorite_hidden':isAdded})} />
            <div className='good-desc-back'>
                <h3 className="good-desc__title">{title ? title : name}</h3>
                <p className="good-desc__subtitle"><i>Артикул: {original_name ? original_name:id}</i></p>
                <p className="good-desc__price">Ціна {price} грн</p>
                <div className="button__wrapper">
                    <Button underlineView onClick={() => {
					handleBasket(item)
					handleModalImage()} }>додати у кошик</Button>
                </div>
            </div>
        </div>
        

        </>
        
    )
}


export default GoodsItem
