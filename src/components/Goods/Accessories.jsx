import {Swiper,SwiperSlide} from 'swiper/react';
import "swiper/css"
import GoodsItem from './GoodsItem';

const Accessories = ({item, date, handleFavorites, favorites, handleBasket, handleCurrentPost, handleModalImage})=>{
    const goodsItem = date.map((item,index)=>(
    <SwiperSlide className="good__item">
        <GoodsItem item={item} key={index} handleFavorites={handleFavorites} favorites={favorites} handleBasket={handleBasket} handleCurrentPost={handleCurrentPost} handleModalImage={handleModalImage} />
    </SwiperSlide>
    ))

    return(
        <Swiper
            slidesPerView={5}
            spaceBetween={16}
            className="goods__wrapper"
            navigation={true}
            grabCursor={false}
            draggable={false}
            preventClicksPropagation={true}
            preventClicks={true}
            scrollbar={{draggable: false, hide: true}}
            slideToClickedSlide={false}
            pagination={{clickable: true}}
        >
         {goodsItem}
        </Swiper>
    )
}

export default Accessories
