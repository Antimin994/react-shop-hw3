import Button from "../../Button/Button";
import {useState} from "react";
import cx from "classnames";
import "./Basket.scss"

const BasketItem = ({basket, handleModal, removeBasket, minusItem, plusItem})=>{
    let {good_img, title, original_title, price, id, original_name, name, quantity} = basket
    const linkPath = original_title ? `/cloth/${id}` : `/accessory/${id}`

    console.log(basket, quantity, basket.quantity);
    console.log(basket.id);
    console.log(removeBasket);
    console.log(handleModal);
    
    return (
        <>
           <div className="basket-desc">
                      <img src={good_img} height={'376px'} alt={title ? title : name}/>
                        <div className='basket-desc-back'>
                            <h3 className="basket-desc__title">{title ? title : name}</h3>
                            <div className="button__counter">
                               <Button boxView onClick={()=>minusItem(basket, quantity)} className="btn_counter">-</Button>
                               <span className="text_counter">{quantity === undefined ? 1 : quantity}</span>
                               <Button onClick={()=>plusItem(basket, quantity)} boxView className="btn_counter">+</Button>
                            </div>
                            <p className="basket-desc__subtitle"><i>Артикул: {original_name ? original_name:id}</i></p>
                            <p className="basket-desc__price">Ціна {price} грн</p>
                            <div className="button__wrapper">
                                <Button boxView >купити</Button>
                                <Button underlineView onClick={() => {
                                    removeBasket(basket)
                                    handleModal()} }>видалити</Button>
                            </div>
                        </div>
            </div>

        </>
        
    )
}


export default BasketItem
