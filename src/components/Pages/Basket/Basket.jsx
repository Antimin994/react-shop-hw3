import react from 'react'
import cx from "classnames";
import PropTypes from 'prop-types';
import BasketItem from './BasketItem';
import "./Basket.scss"
import ModalBase from '../../Modal/ModalBase';


const Basket = (props) => {
   const {baskets, removeBasket, handleModal, isModal, handleConfirmed, minusItem, plusItem} = props;
   let totalPrice = 0;
   const basketItem = baskets.map((basket, index)=>{
    !basket.quantity ? basket.quantity = 1 : basket.quantity;
    totalPrice = totalPrice + parseFloat(basket.price) * parseFloat(basket.quantity);

    return (
        <div className="basket__item">
            <BasketItem basket={basket} key={index} handleModal={handleModal} removeBasket={removeBasket} minusItem={minusItem} plusItem={plusItem} />
       </div>
    )       
    })

  return (
    <div className="container">
			<div className="basket__container">
				<div className="basket__title">Кошик</div>
				<div className="basket__wrapper">{basketItem}</div>
                <ModalBase  isOpen={isModal} handleClose={handleModal} handleOk={handleConfirmed} />
                <div className="basket__total">Всього до сплати: {totalPrice} грн</div>
			</div>
	</div>
  )
}

export default Basket