import Button from "../../Button/Button";
import {useState} from "react";
import cx from "classnames";
import "./Favorite.scss"

const FavoriteItem = ({favorite, handleFavorites})=>{
    const {good_img, title, original_title, price, id, original_name, name} = favorite
    const linkPath = original_title ? `/cloth/${id}` : `/accessory/${id}`
    
    return (
        <>
           <div className="favorite-desc">
                      <img src={good_img} height={'376px'} alt={title ? title : name}/>
                        <div className='favorite-desc-back'>
                            <h3 className="favorite-desc__title">{title ? title : name}</h3>
                            <p className="favorite-desc__subtitle"><i>Артикул: {original_name ? original_name:id}</i></p>
                            <p className="favorite-desc__price">Ціна {price} грн</p>
                            <div className="button__wrapper">
                                <Button boxView >до кошика</Button>
                                <Button underlineView onClick={() => {
                                    handleFavorites(favorite)} }>видалити</Button>
                            </div>
                        </div>
            </div>

        </>
        
    )
}


export default FavoriteItem
