import react from 'react'
import cx from "classnames";
import PropTypes from 'prop-types';
import FavoriteItem from './FavoriteItem';
import ModalBase from '../../Modal/ModalBase';
import "./Favorite.scss"


const Favorite = ({favorites, handleFavorites}) => {
   const favoriteItem = favorites.map((favorite, index)=>{
    return (
        <div className="favorite__item">
            <FavoriteItem favorite={favorite} key={index} handleFavorites={handleFavorites} />
       </div>
    )       
    })


  return (
    <div className="container">
			<div className="favorite__container">
				<div className="favorite__title">Обране</div>
				<div className="favorite__wrapper">{favoriteItem}</div>
			</div>
	</div>
  )
}

export default Favorite